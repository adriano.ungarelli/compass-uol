import faker from 'faker';

Cypress.Commands.add('novo_usuario', function () {
    cy.url().then((url) => {

        if (url.includes('www.natura.com.br')) {
            cy.dados_basicos()
            cy.get('input#pushOptInWP').check()
            cy.get('#acceptedterms').click()
            cy.contains('Criar Conta').click()
            cy.get('i.material-icons.MuiIcon-root.natds-icons-filled-navigation-menu', { timeout: 6000 }).click()
            cy.get('h6.MuiTypography-root.MuiTypography-h6').should('contain', 'Olá,')
            cy.contains('Sair', { timeout: 6000 }).click()
            cy.url().should('eq', 'https://www.natura.com.br/')

        } else if (url.includes('www.thebodyshop.com.br')) {
            cy.dados_basicos()
            cy.get('input#acceptedterms').check()
            cy.contains('Criar Conta').click()
            cy.get('i.material-icons.MuiIcon-root.natds-icons-filled-navigation-menu', { timeout: 6000 }).click()
            cy.get('h6.MuiTypography-root.MuiTypography-h6').should('contain', 'Olá,')
            cy.contains('Sair', { timeout: 6000 }).click()
            cy.url().should('contains', 'https://www.thebodyshop.com.br/')
        }
    })
})

// Preenche os campos comuns aos cadastros natura e bodyshop
Cypress.Commands.add('dados_basicos', function () {
    const senha = generateFakePassword()
    cy.get('input[name="firstName"]').type(faker.name.firstName())
    cy.get('input[name="lastName"]').type(faker.name.lastName())
    cy.get('input[name="email"]').type(faker.internet.email())
    cy.get('input[name="password"]').type(senha)
    cy.get('input[name="confirmPassword"]').type(senha)
    cy.get('input[name="cpf"]').type(generateValidCPF())
    cy.get('input[name="dateOfBirth"]').type(generateFakeBirthdate())
    cy.get('input[name="gender"][value="noSpecify"]').click()
    cy.get('input[name="homePhone"]').type(generateCelphone())
})

// Função para gerar um CPF válido aleatório
function generateValidCPF() {
    const randomCPF = Array.from({ length: 9 }, () => Math.floor(Math.random() * 10));
    const firstVerifierDigit = calculateVerifierDigit(randomCPF);
    const secondVerifierDigit = calculateVerifierDigit([...randomCPF, firstVerifierDigit]);
    return `${randomCPF.join('')}${firstVerifierDigit}${secondVerifierDigit}`;
}

// Função para calcular o dígito verificador do CPF
function calculateVerifierDigit(digits) {
    let sum = 0;
    let multiplier = digits.length + 1;
    for (const digit of digits) {
        sum += digit * multiplier;
        multiplier--;
    }
    const remainder = sum % 11;
    return remainder < 2 ? 0 : 11 - remainder;
}

function generateFakeBirthdate() {
    const fakeBirthdate = faker.date.between('1970-01-01', '2003-12-31'); // Pode ajustar o intervalo de datas conforme necessário
    // Formatar a data como uma string no formato "dd/mm/aaaa"
    const formattedBirthdate = `${fakeBirthdate.getDate().toString().padStart(2, '0')}${(fakeBirthdate.getMonth() + 1).toString().padStart(2, '0')}${fakeBirthdate.getFullYear()}`;
    return formattedBirthdate;
}

function generateCelphone() {
    const prefix = "61";
    const remainingDigits = faker.random.number({ min: 0, max: 99999999 }).toString().padStart(9, '0');
    return prefix + remainingDigits;
}

function generateFakePassword() {
    // Caracteres válidos para a senha
    const specialChars = '!@#$%';
    const numbers = '0123456789';
    const upperCaseChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const lowerCaseChars = 'abcdefghijklmnopqrstuvwxyz';

    // Escolha aleatoriamente um caractere especial
    const specialChar = faker.random.arrayElement(specialChars);

    // Escolha aleatoriamente um número
    const number = faker.random.arrayElement(numbers);

    // Escolha aleatoriamente um caractere maiúsculo e um caractere minúsculo
    const upperCaseChar = faker.random.arrayElement(upperCaseChars);
    const lowerCaseChar = faker.random.arrayElement(lowerCaseChars);

    // Gere uma string aleatória com os caracteres restantes
    const remainingChars = faker.random.alphaNumeric(4); // 4 caracteres aleatórios

    // Combine todos os elementos para criar a senha
    const password = specialChar + number + upperCaseChar + lowerCaseChar + remainingChars;
    const passwordString = password.toString();
    return passwordString;
}