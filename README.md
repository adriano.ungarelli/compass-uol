# compass-uol

## Descrição
Automação de testes de Frontend
- cadastro de novo usuário em:
-> www.natura.com.br
-> www.thebodyshop.com.br

## Antes de iniciar a automação

### Indicar no .gitignore os diretórios e arquivos que não devem entrar no controle de versões

  Por exemplo:
  ```
  .DS_Store
  node_modules/
  cypress/downloads/
  cypress/screenshots/
  cypress/videos/
  ```
### Utilizar o faker para gerar dados necessários aos cadastros

    ```
    npm install faker --save-dev
    import faker from 'faker'
    crie as funções necessárias
    ```
### Para desabilitar o xhr na execução

    ```
    instalar: npm i cypress-plugin-xhr-toggle -D
    no arquivo "e2e.js", adicionar -> import 'cypress-plugin-xhr-toggle'
    no arquivo "cypress.config.js", adicionar -> env: {hideXhr: true}
    ```
### Executar em modo headless

    ```
    - npm run report
    - npx cypress run --spec "cypress\e2e\natura.cy.js" --headless
    - npx cypress run --spec "cypress\e2e\thebodyshop.cy.js" --headless
    ```